package be.kdg.layouts.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.layouts.R
import be.kdg.layouts.databinding.StudiesItemBinding

class StudiesAdapter(var studies: Array<String>) : RecyclerView.Adapter<StudiesAdapter.ViewHolder>() {
  class ViewHolder(var binding: StudiesItemBinding) :RecyclerView.ViewHolder(binding.root)

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudiesAdapter.ViewHolder {
    val binding=StudiesItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
    return ViewHolder(binding)
  }

  override fun onBindViewHolder(holder: StudiesAdapter.ViewHolder, position: Int) {
    holder.binding.studyName.text=studies[position]
  }

  override fun getItemCount() = studies.size

}
