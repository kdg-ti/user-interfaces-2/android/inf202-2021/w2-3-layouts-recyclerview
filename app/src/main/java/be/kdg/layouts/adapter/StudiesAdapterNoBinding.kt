package be.kdg.layouts.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import be.kdg.layouts.R

class StudiesAdapterNoBinding(var studies: Array<String>) : RecyclerView.Adapter<StudiesAdapterNoBinding.ViewHolder>() {
  class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
    val studyName = itemView.findViewById<TextView>(R.id.studyName)
  }


  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudiesAdapterNoBinding.ViewHolder {
    val view=LayoutInflater.from(parent.context).inflate(R.layout.studies_item,parent,false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: StudiesAdapterNoBinding.ViewHolder, position: Int) {
    holder.studyName.text=studies[position]
  }

  override fun getItemCount() = studies.size

}
